/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2019 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of this software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package nxt.blockchain.chaincontrol;

import org.junit.Test;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static nxt.blockchain.chaincontrol.PermissionPolicyType.CHILD_CHAIN;
import static nxt.blockchain.chaincontrol.PermissionPolicyType.NONE;
import static nxt.http.APITag.PERMISSION_POLICY_CHILD_CHAIN;
import static org.junit.Assert.assertEquals;

public class PermissionPolicyTypeTest {

    @Test
    public void getBlockedAPITags() {
        assertEquals(emptyList(), CHILD_CHAIN.getDisabledAPITags());
        assertEquals(singletonList(PERMISSION_POLICY_CHILD_CHAIN), NONE.getDisabledAPITags());
    }
}