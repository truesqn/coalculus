/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2019 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of this software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package nxt.blockchain.chaincontrol;

import nxt.blockchain.ChildChain;
import nxt.http.APITag;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PermissionPolicyType {

    NONE() {
        @Override
        public PermissionPolicy create(ChildChain childChain) {
            return NonePermissionPolicy.POLICY_INSTANCE;
        }
    },

    CHILD_CHAIN(APITag.PERMISSION_POLICY_CHILD_CHAIN) {

        {
            ChildChainPermissionStore.init();
        }

        @Override
        public PermissionPolicy create(ChildChain childChain) {
            return new ChildChainPermissionPolicy(this.name(), childChain);
        }
    };

    private final Collection<APITag> enabledAPITags;

    PermissionPolicyType(APITag... enabledAPITags) {
        this.enabledAPITags = Arrays.asList(enabledAPITags);
    }

    public Collection<APITag> getDisabledAPITags() {
        return Stream.of(values())
                .filter(t -> t != this)
                .flatMap(t -> t.enabledAPITags.stream())
                .filter(t -> !this.enabledAPITags.contains(t))
                .collect(Collectors.toList());
    }

    public abstract PermissionPolicy create(ChildChain childChain);
}
